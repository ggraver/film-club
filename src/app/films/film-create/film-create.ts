import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { User } from 'src/app/users/user';

export class FilmCreate {
  public title: string;
  public user: User;
  public timestamp: NgbDate;
}
