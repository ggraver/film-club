import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FilmListComponent } from './films/film-list/film-list.component';
import { HomeComponent } from './home/home.component';
import { FilmDetailComponent } from './films/film-detail/film-detail.component';
import { FilmDetailResolverService } from './films/film-detail/film-detail-resolver.service';


const routes: Routes = [
  {
    path: 'films/:id',
    component: FilmDetailComponent,
    resolve: {
      film: FilmDetailResolverService
    }
  },
  { 
    path: 'films',
    component: FilmListComponent,
  },
  { path: 'home', component: HomeComponent },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
