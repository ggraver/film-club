import { Injectable } from "@angular/core";
import { Router, Resolve, ActivatedRouteSnapshot } from "@angular/router";
import { Observable, of, EMPTY } from "rxjs";
import { mergeMap, take } from "rxjs/operators";

import { Film } from "src/app/films/film";
import { FilmService } from "src/app/films/film.service";

@Injectable({
  providedIn: "root"
})
export class FilmDetailResolverService implements Resolve<Film> {
  
  constructor(
    private filmService: FilmService,
    private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Film> | Observable<never> {
    const id: string = route.paramMap.get("id");

    return this.filmService
      .get(id)
      .pipe(
        take(1),
        mergeMap(film => {
          if (!film) {
            this.router.navigate(["films"]);
            return EMPTY;
          }

          return of(film);
        })
      );
  }
}
