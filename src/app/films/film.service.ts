import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Film } from './film';
import { AngularFirestore } from '@angular/fire/firestore';

import { map } from 'rxjs/operators';
import { FilmCreate } from './film-create/film-create';

@Injectable({
  providedIn: 'root'
})
export class FilmService {
  constructor(private db: AngularFirestore) { }

  public list(): Observable<Film[]> {
    return this
      .db
      .collection<Film>('films')
      .snapshotChanges()
      .pipe(map(actions =>
        actions.map(action => {
          return { 
            id: action.payload.doc.id,
            ...action.payload.doc.data()
          }
        })));
  }

  public get(filmId: string): Observable<Film> {
    return this.db
      .doc<Film>(`films/${filmId}`)
      .snapshotChanges()
      .pipe(map(action => {
        return {
          id: action.payload.id,
          ...action.payload.data()
        }
      }));
    }

  public async create(film: FilmCreate): Promise<void> {
    await this
      .db
      .collection('films')
      .add({
        title: film.title,
        user: film.user,
        timestamp: new Date(
          film.timestamp.year,
          film.timestamp.month,
          film.timestamp.day)
      });
  }

  public async delete(filmId: string): Promise<void> {
    await this.db
      .collection("films")
      .doc(filmId)
      .delete();
  }
}
