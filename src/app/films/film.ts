import { Timestamp } from '@firebase/firestore-types';
import { User } from '../users/user';

export class Film {
  public id: string;
  public title: string;
  public timestamp: Timestamp;
  public user: User;
}
