import { Component, Input, ViewChild } from '@angular/core';
import { Film } from '../film';
import { ModalComponent } from 'src/app/modal/modal.component';
import { FilmService } from '../film.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-film-delete',
  templateUrl: './film-delete.component.html',
  styleUrls: ['./film-delete.component.scss']
})
export class FilmDeleteComponent {
  @ViewChild(ModalComponent, { static: false }) private modal: ModalComponent;
  public film: Film;

  constructor(
    private filmService: FilmService,
    private router: Router) { }

  public async delete(): Promise<void> {
    await this.filmService.delete(this.film.id);
    this.modal.close();
    this.router.navigate(["", "films"]);
  }
}
