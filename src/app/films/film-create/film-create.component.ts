import { Component, ViewChild, OnInit } from "@angular/core";
import { FilmService } from "../film.service";
import { FilmCreate } from "./film-create";
import { ModalComponent } from "src/app/modal/modal.component";
import { User } from 'src/app/users/user';
import { UsersService } from 'src/app/users/users.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: "app-film-create",
  templateUrl: "./film-create.component.html",
  styleUrls: ["./film-create.component.scss"]
})
export class FilmCreateComponent implements OnInit {
  @ViewChild(ModalComponent, { static: false }) private modal: ModalComponent;
  public film: FilmCreate = new FilmCreate();

  public users: Observable<User[]>;

  constructor(
    private filmService: FilmService,
    private userService: UsersService) {}
  
  ngOnInit(): void {
    this.users = this.userService.list();
  }

  public async onSubmit(): Promise<void> {
    await this.filmService.create(this.film);
    this.modal.close();
  }
}
