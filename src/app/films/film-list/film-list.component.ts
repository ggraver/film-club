import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Film } from '../film';
import { FilmService } from '../film.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FilmCreateComponent } from '../film-create/film-create.component';

@Component({
  selector: 'app-film-list',
  templateUrl: './film-list.component.html',
  styleUrls: ['./film-list.component.scss']
})
export class FilmListComponent implements OnInit {
  public films: Observable<Film[]>;
  
  constructor(
    private filmService: FilmService,
    private modalService: NgbModal) {}

  ngOnInit(): void {
    this.films = this.filmService.list();
  }

  public addFilm(): void {
    const modalRef = this.modalService.open(FilmCreateComponent);
  }
}
