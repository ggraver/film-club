import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HomeComponent } from './home/home.component';
import { FilmListComponent } from './films/film-list/film-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FrameComponent } from './frame/frame.component';
import { FilmCreateComponent } from './films/film-create/film-create.component';
import { ModalComponent } from './modal/modal.component';
import { FormsModule } from '@angular/forms';
import { FilmDetailComponent } from './films/film-detail/film-detail.component';
import { FilmDeleteComponent } from './films/film-delete/film-delete.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FilmListComponent,
    FrameComponent,
    FilmCreateComponent,
    ModalComponent,
    FilmDetailComponent,
    FilmDeleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    FilmCreateComponent,
    FilmDeleteComponent
  ]
})
export class AppModule { }