import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private db: AngularFirestore) { }

  public list(): Observable<User[]> {
    return this
      .db
      .collection<User>('users')
      .snapshotChanges()
      .pipe(map(actions =>
        actions.map(action => {
          return { 
            id: action.payload.doc.id,
            ...action.payload.doc.data()
          }
        })));
  }
}
