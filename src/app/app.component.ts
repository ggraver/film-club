import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Film } from './films/film';
import { FilmService } from './films/film.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ui';
  public films: Observable<Film[]>;
  
  constructor(private filmService: FilmService) {}

  ngOnInit(): void {
    this.films = this.filmService.list();
  }
}
