import { Component, OnInit, Input } from '@angular/core';
import { Film } from 'src/app/films/film';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { FilmDeleteComponent } from '../film-delete/film-delete.component';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.scss']
})
export class FilmDetailComponent implements OnInit {
  @Input() public film: Film = new Film();

  constructor(
    private route: ActivatedRoute,
    private modal: NgbModal) { }

  ngOnInit() {
    this.route.data
      .subscribe((data: { film: Film }) => {
        this.film = data.film;
      });
  }

  public deleteFilm(): void {
    const modalReference: NgbModalRef<typeof FilmDeleteComponent> = this.modal.open(FilmDeleteComponent);
    modalReference.componentInstance.film = this.film;
  }
}
